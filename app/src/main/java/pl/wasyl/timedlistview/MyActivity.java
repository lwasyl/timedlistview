package pl.wasyl.timedlistview;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.SeekBar;


public class MyActivity extends Activity implements SeekBar.OnSeekBarChangeListener {

    private static final String TAG = MyActivity.class.getSimpleName();

    CustomListView listView;
    ListAdapter adapter;

    SeekBar seekBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        listView = (CustomListView) findViewById(R.id.listView);

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, new String[]{
                "lorem", "ipsum", "dolor", "sit", "amet",
                "lorem", "ipsum", "dolor", "sit", "amet",
                "lorem", "ipsum", "dolor", "sit", "amet",
                "lorem", "ipsum", "dolor", "sit", "amet",
                "lorem", "ipsum", "dolor", "sit", "amet",
                "lorem", "ipsum", "dolor", "sit", "amet",
                "lorem", "ipsum", "dolor", "sit", "amet"
        });
        listView.setAdapter(adapter);

        int markedRow = 3;
        float markerPosition = 0.9f;

        listView.setMarkedRow(markedRow);
        listView.setMarkerPosition(markerPosition);

        seekBar = (SeekBar) findViewById(R.id.seekBar);

        seekBar.setMax(adapter.getCount() * 100);
        seekBar.setProgress((int) ((markedRow + markerPosition) * 100));
        seekBar.setOnSeekBarChangeListener(this);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        switch (seekBar.getId()) {
            case R.id.seekBar:
                Log.d(TAG, String.format("Marking row %d at %d%% height", progress / 100, (progress % 100)));
                listView.setMarkedRow(progress / 100);
                listView.setMarkerPosition((progress % 100) / 100f);
                listView.invalidate();
                // we can make our own invalidate that only invalidates rectangles of previously marked row and currently marked row
                // (and probably even only in the rectangle where we drew the line)
                break;
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
