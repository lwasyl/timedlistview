package pl.wasyl.timedlistview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ListView;

/**
 * Created by Łukasz Wasylkowski on 2014-09-19.
 */
public class CustomListView extends ListView {

    private int markedRow = 0;
    private float markerPosition = 0.0f;
    Drawable drawable;

    Paint paint = new Paint();
    Rect childRect = new Rect();

    public CustomListView(Context context) {
        super(context);
        init();
    }

    public CustomListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        drawable = getContext().getResources().getDrawable(R.drawable.line);
        paint.setColor(Color.BLACK);
    }

    public void setMarkedRow(int markedRow) {
        this.markedRow = markedRow;
    }

    public void setMarkerPosition(float markerPosition) {
        this.markerPosition = markerPosition;
    }

    @Override
    protected boolean drawChild(Canvas canvas, View child, long drawingTime) {

        boolean rv = super.drawChild(canvas, child, drawingTime);

        if (getPositionForView(child) == markedRow) {
            child.getDrawingRect(childRect); // relative rectangle, size of the child view(so top-left is 0,0)
            childRect.top -= getDividerHeight(); // so that 0% starts at the divider exactly
            float absoluteLinePosition =
                    child.getTop() + // child position in parent
                            +(childRect.bottom - childRect.top) * markerPosition; // height * line position
            // 20px top and bottom so that drawable has height (we have fixed-height line anyway)
            // could be set at setMarkHeight or sth
            drawable.setBounds(childRect.left, (int) absoluteLinePosition - 20, childRect.right, (int) absoluteLinePosition + 20);
            drawable.draw(canvas);
        }

        return rv;
    }
}
